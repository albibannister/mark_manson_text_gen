Eighty years ago, researchers began one of the longest and most complicated projects to understand human behavior in history. It would take almost 50 years to complete. But their work would define an entire field of psychology. 

It started with an idea: that people have different fundamental character traits and these character traits are inherited and stable throughout one’s life. It was the idea of personality.
The problem was that there were an infinite number of human behaviors, so how could you know what was caused by someone’s personality, and what was caused by all the shit going on around them?
To test and find stable personality traits, researchers would have to make an exhaustive list of all of the possible human behaviors and then measure these behaviors in a lot of people over a very long time to determine what was fundamental personality and what was just noise and bullshit.
The project started out humbly enough. In 1936, Gordon Allport and Henry Odbert pulled out a dictionary and went through every single entry, writing down any word that could potentially describe human behavior.1
Talk about a one-way train ticket to Boresville.
Henry Allport—dude really loved his dictionaries.
But this work was actually very important because our understanding of human behavior is bounded by words. Therefore, to get a full sample of pre-defined human behavior, you have to go through the whole damn dictionary.
In the end, they assembled a list of 4,500 words that describe all the crap humans do. Everything from “fetishize” to “bootlicking” to “scatological” were probably on the list… well, that is, assuming those words were in the dictionary back then.
The next project was to start going through the list and grouping them into large categories that encompassed as many words as possible. Words like “talkative,” “wordy,” “loquacious,” and “garrulous” could all be thrown under the “talkative” umbrella. Words like “mopey,” “whiny,” and “self-pitying” could all be thrown under a “melancholy” umbrella. And so on.
This took almost 10 years. Then another psychologist named Raymond Cattell came along, and based on Allport and Odbert’s research, declared 16 fundamental personality traits that defined all human behavior.2 But as time went on, it became clear that many of these traits came and went in people over time and/or based on their circumstances, and other traits remained relatively consistent over time. Each time a trait was observed to fluctuate too much, psychologists threw it out.
What was left in the 1960s was five stable traits: extraversion, openness to new experience, agreeableness, conscientiousness, and neuroticism. These five categories were deemed to be able to explain all human behavior. But it would be another 20 years before they had sufficient research and data to back up this claim.
By the 1990s, we had data and everyone threw a party. Well… the extraverts threw a party. And then people high in neuroticism got really anxious and fussed about what to wear.
The point is, these five traits have since become known as the Big Five Personality Traits, and they are one of the most established and scientifically-driven measurements in the field of psychology. Which, in case you haven’t been paying attention, has very few established and scientifically-driven measurements. 3
The Big Five are relatively stable over time.4 They persist through circumstance. There’s a significant genetic component. They partially determine who you are, the choices you make, and how well you do in life.
On average, extraverts experience more positive emotions, have wider social networks, and, likely as a result, make more money. People who are conscientious are healthier and live longer, probably because they wash their hands after they piss. People with high levels of neuroticism struggle emotionally and are more likely to lose jobs, get divorced, and become depressed. People who are more open to experience tend to be creative, risk-taking, and political liberals. People who are low in openness to experience tend to be politically conservative and bad at hosting orgies.
But out of all of the Big Five Personality Traits, one of the five stands above them all in determining professional success: agreeableness.
Or rather, a lack of agreeableness.5
Basically, assholes make more money. Often, a lot more money.
Now, it’s easy to lament that this is just more evidence of how fucked up the world is, and how the meanest and cruelest in our society are always the ones who get ahead.
But I think that’s a whiny and immature way to look at it. Just because you are nice doesn’t mean you are good. And just because someone is mean doesn’t mean they’re a bad person.
In fact, I believe the world needs its fair share of assholes. And that being an asshole is a valuable life skill. What I mean by “being an asshole” is a willingness to be disliked and/or to upset other people. As we will see, sometimes hurting someone’s feelings (or just being willing to hurt someone’s feelings) is a necessity, both for ourselves and also for the greater good. And I believe that if more of us were able and willing to “flip the asshole switch,” the world would be a better place.
The Game Theory of Asshole-dom
Let’s say you have two sides of a business deal. Let’s also assume this is a big, important business deal that will potentially generate a lot of money for all involved and is also good for the world.
Now, let’s say that one side has learned the ineffable skill of being an asshole and the other has not. That is, one side is totally willing to be disliked and the other is not.
What’s going to happen? Well, it’s obvious: the asshole is going to steamroll the other side and get a deal that heavily favors them. Run this situation millions of times over the course of multiple decades and you end up in a situation where assholes run the world. No surprise.
But let’s run the experiment again with two non-assholes—two people unwilling to be disliked.
Both sides come together, and instead of pushing for every small advantage for their side, they don’t want to be assholes, so they agree to terms that are fine but not stellar for themselves. The deal still gets done, but that deal will be sub-optimal because neither side pushed to the full extent of their capabilities. Therefore, a lot of value will be lost in the process.
In other situations, two non-assholes will fail to reach a deal because the initial offers on both sides will be so far off the mark and they won’t want to push too hard to make the other side dislike them. Instead, both will kindly say, “Hey! I guess it’s not going to work out! But no hard feelings, let’s get a drink and play Yahtzee!” And they will play Yahtzee… for play money because they will be broke.
The third situation is when two assholes come to a business negotiation. Both sides are totally willing to be disliked. And not only will they push for everything they need for the deal to be advantageous for them, but they will push even further. They will consciously antagonize the other side because they understand that antagonism wears people down and makes them capitulate more easily.
Strangely, this really, really unpleasant circumstance is the one that will produce the most optimal result. Both sides will likely push the deal so far that no one ends up happy with it. Both sides will feel like they lost but the resulting agreement will likely produce better results for both sides because they left no stone unturned in pursuing what is best for themselves.




Asshole
Nice Person


Asshole
Optimal Deal — but everyone hates each other
Asshole gets more than she deserves — but everyone still gets along


Nice Person
Asshole gets more than he deserves — but everyone still gets along
Sub-optimal deal — but everyone likes each other



So, yes, assholes run the world. But that’s because in high stakes situations, being an asshole is advantageous. Sometimes it’s useful for your boss to think you’re kind of a dick (ever hear the saying, “He’s an asshole, but he’s our asshole?”). Sometimes it’s useful for your friends to think you’re an asshole (strangely, it shows them they can trust you). And ever tried to break up with someone while not hurting their feelings? Yeah, it’s impossible. So most non-assholes just end up staying in bad relationships for way longer than they should.
So, let’s summarize:

The more important something is, the higher the stakes.
The higher the stakes, the more emotionally involved people are going to be with the outcome.
The more emotionally involved people are with the outcome, the more difficult it will be to upset someone or tell them something they don’t want to hear.
Therefore, the more important something is, the more valuable it is to be able to shit in somebody’s mouth if necessary.

QED: Learn to be an asshole
The best way to do business…apparently.
This is an uncelebrated skill. Hell, it’s a skill that we look down on as a society and a culture because it’s unpleasant. But it’s a necessary part of the world. That is, assuming the asshole has a code of ethics.
How to Be an Ethical Asshole
When we think of assholes we don’t like, we think of people who are unethical. They lie, cheat, or steal to get their way.
Yes, these people are assholes. But they are also unethical. Let’s put this in terms of an SAT question:

All unethical people are unlikeable
All unlikeable people are assholes

TRUE or FALSE: All assholes are unethical.

A: TRUE, I’m bad at logic
B: TRUE, fuck you Manson! Only I decide what’s true!
C: NEITHER, this question violates my religious beliefs
D: FALSE, while all unethical people are assholes, not all assholes are unethical

The correct answer is ‘D’.
Yes, there is such thing as an ethical asshole. And, I would argue, ethical assholes are national treasures. We need ethical assholes because they’re the only thing protecting us from the unethical assholes.
So, assuming you’re ethical, how does one become more of an asshole? 6
Well, as we’ve established, some people are born with it. Some people are just naturally very disagreeable. They think people are pretty shitty, in general, so they don’t care if people don’t like them.
But for those who are agreeable, learning to be an asshole is a skill that must be practiced. The same way an introvert must practice using extraverted skills when necessary, the agreeable person must learn to be disagreeable when necessary, lest they get walked over.
Here are a few steps to becoming more of an asshole:
1. Decide what’s more important than people’s feelings
Most people let their life be dictated by feelings—both their own feelings and others’. They don’t even realize this because they haven’t stopped to think about it. But, as I’ve written before, if you allow your life to be dictated by feelings, then you’re going to be stuck in a shithole forever.
The key to developing the willingness to upset other people is to understand what’s more important than them being upset. Would you hurt someone’s feelings to save a dying family member? Yeah, probably. What about to save your career? I would hope so (some people probably don’t). What about to promote a good cause you care about?
Unethical assholes are assholes because they care about themselves more than others. They are narcissistic and see the world only in terms of what benefits themselves. Obviously, that’s bad. They are unethical because their cause is bad. Finding a good cause beyond one’s own interests is the first step to becoming an ethical asshole.
2. Get good at feeling bad
Most people who are too nice think they are nice because they care too much about other people’s feelings. They say to themselves, “Well, I could never say that to her, because she’ll feel bad.” But they’re lying to themselves. They think they’re being altruistic, but they’re not.
They don’t want to say it to her because they’ll make themselves feel bad. Their sympathy for others is a tool that holds them back. They won’t hurt others’ feelings because then they’ll feel bad for that person and they can’t stand to feel bad themselves.
Get good at feeling bad and you’ll become okay at making others feel bad when necessary. I just got off the phone with a friend last night. I chewed him out for doing something pretty stupid that involved me. He felt awful. I feel awful that he feels awful. But I also know that it’s a good thing that we both feel awful. It’s for a good cause. So I can bear it.
But for me to be able to make him feel awful, I first need to be capable of feeling awful myself.
3. Lean in to painful honesty
We’ve all been in that situation where we want to say something that’s important but there’s also a good chance that we’ll upset someone if we say it. There’s that uncomfortable tension inside us as we go back and forth on whether we should say it or not.
Create a new rule for yourself: if there’s something uncomfortable that you believe is important to say, just say it. Don’t think about it. Just trust that in the long run, more times than not, you’ll be happy that you said it. In fact, chances are, in the long run, other people will be glad you said it.
The first few times you’re an asshole in this way, it’ll feel terrifying. But once you get some of that positive social feedback, you’ll start feeling more comfortable with it. And it will come more naturally. You’ll be an asshole. But you’ll be their asshole.
Because here’s the funny thing you’ll notice once you start to hone your asshole skills: other people will come up to you privately, once everyone else has left the room, or maybe corner you in a remote hallway somewhere, and, while looking over their shoulders to make sure no one else can hear, they’ll say, “Hey, thank you for saying that. My god, that really needed to be said. I’m so glad you did.”
This will begin to happen all the time. In fact, it’s shocking how much non-assholes rely on ethical assholes to go to bat for them.
You know, kind of like what I’m doing now, you candy-ass snowflakes. Seriously people, you’re fucking welcome. You know how many times I’ve stuck my neck out for shit you care about but are too scared to say? Christ on a cupcake. Maybe get off your asses every once in a while, eh? Now, get the fuck out of here. I have a book to write.

Footnotes

Allport, G. W., & Odbert, H. S. (1936). Trait-names: A psycho-lexical study. Psychological Monographs, 47(1), i.↵
Cattell, R. B., Eber, H. W., & Tatsuoka, M. M. (1970). Handbook for the 16 personality factor questionnaire. Champaign, IL: Institute for Personality and Ability Testing.↵
Of course, there is some disagreement about the model, and some recent research suggests there might be six traits instead of five. But psychologists generally agree that The Big Five captures the human experience well.↵
All of The Big Five appear to change somewhat over the lifespan over the course of decades. But in the short- to medium-term, personality traits are generally very stable.
See: Srivastava, S., John, O. P., Gosling, S. D., & Potter, J. (2003). Development of personality in early and middle adulthood: Set like plaster or persistent change? Journal of Personality and Social Psychology, 84(5), 1041.
And: Cobb-Clark, D. A., & Schurer, S. (2012). The stability of big-five personality traits. Economics Letters, 115(1), 11–15.↵
Heineck, G. (2011). Does it pay to be nice? Personality and earnings in the United Kingdom. Industrial and Labor Relations Review, 64(5), 1020–1038.↵
If you’re already an asshole but need help becoming more ethical, then that’s another post for another day.↵



