
Contents

What is “mental health” anyway?
Factors that affect your mental health
How to improve your mental health
More articles on mental health



What is mental health?
For the vast majority of modern medical history, “health” was basically defined as the absence of disease. If you weren’t sick, you were healthy. Mental health was treated the same way: if you weren’t a crazy loon, then you were considered mentally healthy.
But as medicine and psychology advanced, it became clear that our mental health included a wider range of emotional and social factors that don’t necessarily have much to do with mental illness. Mental health became more closely associated with well-being across all domains of our lives—both personally (psychological, emotional, cognitive, etc.) and interpersonally (community and family, romantic relationships, professional fulfillment, etc.).
Our mental health affects our everyday lives, influencing how we respond to stress, how we make decisions, how we interact with others, our sense of fulfillment and purpose in the world, and on and on.
It’s important, then, that we take a step back and really think about our mental health in holistic terms like this, not just as a lack of mental disorder.

Get a better grip on your emotions
Enter your email address below and I’ll send you a 49-page guide with tips on becoming more self-aware, empathetic, and emotionally intelligent.
I also send out occasional updates with new articles and what I’m working on. Your information is safe and I never, ever spam.
 

Factors that affect your mental health
Genetics
Studies have found that our genes play a significant role in our mental health and well-being. Your baseline level of happiness, for instance, is about 50% determined by your genes. If mom and dad were grumpy assholes, well you’ll probably have a tendency to be a grumpy asshole too.1
And, of course, if a close relative of yours has a mental disorder, then you’re also at a higher risk for mental illness as well.2
Now, before you start thinking that this sounds a bit too fatalistic, a significant portion of your mental health is also influenced by environmental factors that you can control.
You might be predisposed to anxiety, but you can take steps to lessen the impact of your anxiety on your life. You can work with a therapist to identify the things that make you anxious and learn coping strategies that help you deal with your anxiety on a day-to-day basis. You can read books and meditate and tap your forehead and chant mantras, or whatever the fuck the kids do these days. The point is: you can actively minimize your anxiety despite your genetics.3
Life Experiences and Trauma
Here’s a shocker: intense negative experiences and trauma can mess you up.
Having nice things like a job you like, friends and family you can count on, and your health is intact, you’re more likely to have good mental health. If you hate yourself, your life and the people in it, well, then your mental health is going to suffer.
Some degree of trauma is unavoidable. We all get screwed in one way or another. Research suggests that virtually everyone experiences at least four or five traumatic experiences in their life.4 This could include everything from losing a job, to losing a loved one, health scares, or, in severe cases, physical and psychological abuse.
The time at which we experience trauma can also influence how big of an impact it has on us. Trauma experienced early in life has a bigger chance of causing problems throughout our lives.5 But trauma as an adult can have a severe effect as well.
But here’s the thing: trauma, while terrible, doesn’t have to fuck us up so bad for so long. In fact, most people don’t see their mental health suffer due to trauma.6 In fact, for most people, it turns out trauma can actually push us to grow as individuals.
More articles on dealing with negative life experiences

How to Grow from Your Pain
Happiness Is Not Enough
9 Steps to Hating Yourself a Little Less
63 Steps to Survive The Worst Moments of Your Life

How to improve your mental health
Below are just a few guidelines to get you to a place where you can manage your mental health more confidently if you often feel out of place, unfulfilled, or like something is “missing” and it causes you some degree of distress at times.
Note, however, that I’m not a therapist and if you or someone you know is struggling with a mental illness, you need to seek help from a licensed healthcare professional (more on that later).
Create deeper social connections
One of the best predictors of good mental health is having a solid social network.7
Money, sex, prestige, status—all of that will only give your mood a short-lived boost. Soon enough, you’ll be back to being the miserable fuck you were. Trust me: do not chase sources of external validation to fill some sort of void you’re feeling. Been there, done that. Got the vomit stains to prove it.

Instead, knowing that you have just a few people—or even one person—you can turn to when shit gets real gives you a social and psychological safety net. The thing is, relationships like these have to be cultivated and fostered over time.
You do this by sharing yourself with others honestly and with vulnerability. This will turn some people off, but that’s okay. You don’t need to make friends with everyone. When it comes to relationships, always strive for quality over quantity.
The people who do respond positively to your honest expression are the people you can start to build your “tribe” with.
Develop healthy lifestyle habits
Look, I know everyone “knows” they should take better care of their bodies, but most of us (myself included) are still pretty terrible about it. And it’s arguably even more important if you’re trying to improve your mental health.
Your mental health will almost always benefit from incorporating healthy lifestyle habits into your day. The whole “mind-body” connection, after all, is only a thing because the brain is a part of the body. So taking care of your body means you’re taking care of your brain too.

The big areas to really, actually, like for real focus on are:
Get some sleep. Seriously. The medical field has finally woke up (pun intended) to the fact that it’s not just a “good idea” to get a solid eight hours of sleep—it’s absolutely critical for your health. Sleep-deprived people who regularly get less than 7-8 hours of sleep per night are not only more irritable and less focused, they also show a lot of the hallmark signs of depression and anxiety.8 A few nights of bad sleep here and there are inevitable, but if you haven’t slept well in years or even a decade or more, it will have outsized effects across your entire life.
Get into a regular sleep routine, limit/eliminate your caffeine and alcohol consumption, and turn the screens off at night.
If you have real trouble sleeping, see a doctor. It might be as simple as figuring out a new routine or you might need more intervention. Either way, it’s worth it.
Solid sleep will also make all the following healthy habits that much easier because, well, you won’t be so damn tired and burnt out when it comes time to do them.
So onward.
Exercise. I probably don’t need to tell you that regular exercise has all sorts of health benefits and makes you feel better and blah blah blah…I’m not going to tell you that exercise is a cure-all for any mental health issues you might be having, but it can definitely start to push your body and mind in the right direction. A short walk can boost your energy levels when you’re dragging a bit, giving you the extra nudge you need to get some work done or get through a setback in your day. Nearly all forms of exercise help relieve stress, especially things like martial arts.
The key is to find exercise that 1) you enjoy and 2) fits into your life. You don’t have to spend hours in a gym to get the benefits of exercise and you don’t have to go completely out of your way to do it either. Daily walks/jogs or simple stretches and calisthenics are often enough to get you the boost you need to work on your mental health.
Once you get into a habit exercising, you can optimize from there.
Healthy eating. Good sleep and regular exercise provide your mind and body a better starting point from which to manage your mental health on a day-to-day basis. Healthy eating habits will help fuel you from there.
Honestly, I’m not the best person to ask for nutritional information, but I will say this: whenever I’ve been on a streak of indulgent eating and drinking, my mental health takes a hit. I’m more tired, I start to feel uneasy, and I find it way easier to slide into bad habits from there, starting a vicious cycle that gets harder and harder to get out of.
There is no need to get complicated with this. In fact, keeping it dead simple and sticking to the basics will make it way more likely that you maintain a healthy diet. So, lean meats (if you eat meat), fruits, and vegetables should make up the vast majority of your diet. Limit fat and carbs, including sugar.
More articles on creating healthy lifestyle habits

The Guide to Habits
Your Goals Are Overrated
If Self-Discipline Feels Difficult, Then You’re Doing It Wrong
How to Stop Procrastinating
How I Quit Smoking For Good (Subscribers Only)
Winning the Mental Battle of Weight Loss: How One Man Lost 266 Pounds
How to Be Patient in an Impatient World
Three Months Later: Does the Attention Diet Work? (Subscribers Only)

Meditate
I have an entire article on meditation—why you should do it, all the benefits, etc.
Briefly, in terms of helping your mental health, meditation can help you identify toxic thought patterns and examine them more closely. It gets you more in tune with your body as well as your self-talk. It can help you slow down and think—really think—and be more present while being less attached to the daily ups and downs of life.

There are literally millions of resources to help you meditate. A few I like are apps like Calm and Headspace. The neuroscientist and famous podcaster Sam Harris also offers a guided course on his app, Waking Up, as well.
Spend time in nature
Outdoor enthusiasts have long touted the health benefits of being in nature, both physical and mental. But scientists—being scientists—thought they needed to run fancy studies with control groups and write really nerdy articles with graphs and tables and overly complicated writing.
Their conclusion: yep, the enthusiasts were right. Spending time in nature does benefit your mental and physical well-being.9
Thanks, scientists.
Seriously though, they’ve found that about two hours per week in nature produced measurably better reports of health and well-being.10 It doesn’t have to be all at once either. It could be as simple as going on a walk through a park near your house (double gold star for combining with exercise) a few times a week.

Whatever it is, nature just has a calming effect on people, especially those of us who spend all of our time in bustling, noisy city environments.
For me, it gives me a better sense of the scale of everything—my life, my problems, etc. You realize you’re a part of something that’s much bigger than yourself, something that will be here long after you’re gone. For a few moments, when I’m in nature, I feel like I can connect with whatever that something is. And it has a way of making all my problems seem smaller.
Getting professional help
There is no magical cure-all for your mental health. But even small changes in the areas I’ve outlined so far can help tremendously.

Even then, you might still feel like you need some outside help. If so, I encourage you to give therapy a try while you continue to develop a healthier lifestyle.
Therapy, too, is not a cure-all for whatever you’re going through, but it can be an effective tool in getting you to a place where you can better manage your mental health.
If this is something you’ve even thought about, check out an article I wrote on getting therapy.
More articles on mental health

Footnotes

Jang, K. L., Livesley, W. J., & Vemon, P. A. (1996). Heritability of the Big Five Personality Dimensions and Their Facets: A Twin Study. Journal of Personality, 64(3), 577–592.↵
Heritability of mental illness varies by disorder, but many show a strong genetic component.↵
Treatments like Cognitive Behavioral Therapy (CBT) and Acceptance and Commitment Therapy (ACT) are highly effective in treating many types of anxiety disorders.↵
See Michaela Hass’s book, Bouncing Forward: The Art and Science of Cultivating Resilience.↵
Past trauma may haunt your future health. Harvard Health.↵
Bonanno, G. A. (2004). Loss, Trauma, and Human Resilience: Have We Underestimated the Human Capacity to Thrive After Extremely Aversive Events? American Psychologist, 59(1), 20–28.↵
Siedlecki, K. L., Salthouse, T. A., Oishi, S., & Jeswani, S. (2014). The Relationship Between Social Support and Subjective Well-Being Across Age. Social Indicators Research, 117(2), 561–576.↵
To learn way more than you ever thought you could about sleep and why it’s so damn important, check out the book Why We Sleep by Matthew Walker.↵
Capaldi, C. A., Passmore, H.-A., Nisbet, E. K., Zelenski, J. M., & Dopko, R. L. (2015). Flourishing in nature: A review of the benefits of connecting with nature and its application as a wellbeing intervention. International Journal of Wellbeing, 5(4).↵
White, M. P., Alcock, I., Grellier, J., Wheeler, B. W., Hartig, T., Warber, S. L., Bone, A., Depledge, M. H., & Fleming, L. E. (2019). Spending at least 120 minutes a week in nature is associated with good health and wellbeing. Scientific Reports, 9(1), 1–11.↵



