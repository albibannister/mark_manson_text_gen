The English philosopher Bertrand Russell once said, “The whole problem with the world is that fools and fanatics are always so certain of themselves, and wiser people so full of doubts.”
Perhaps what’s even more amazing is that he said this long before the advent of the internet.

Today, due to the joys of social media, we are regularly exposed to legions of people who believe they know what the fuck they are talking about when they do not. And, indeed, as Russell pointed out, the more clueless these people are, the more confident in their pronouncements they seem to be.
It turns out that Russell’s axiom has been studied and the data back it up. People who are bad at something do believe they are good at it, and people who are good at it do believe they are bad at it. Amateurs are overconfident and experts are underconfident. Newbies believe they’ve got it all figured out and the weathered veterans understand that nothing is really known for sure.
In psychology, this is known as the “Dunning-Kruger Effect.” It’s a psychological tendency named after the two researchers who initially measured it. And it’s surprising how wide its applications are in our lives.
The Dunning-Kruger Effect: Ignorance of Ignorance
There are four types of information:


Known knowns
Information you know you understand. (e.g., how to ride a bike.)


Known unknowns
Information you know you don’t understand. (e.g., quantum physics.)


Unknown knowns
Information that you know, but you didn’t realize that you knew it. Bonus! (e.g., we didn’t realize we instinctively knew how to be a parent until it happened.)


Unknown unknowns
Information that you’re completely oblivious to. Not only do you not know it, you don’t even know that you don’t know it.


The unknown unknowns are where the Dunning-Kruger effect comes into play in the worst way. It’s our tendency to overestimate our own knowledge/skills/competence and underestimate our own ignorance.1
The Dunning-Kruger Effect goes beyond ignorance. It presents a meta-layer of ignorance—the ignorance of our own ignorance.
It’s one thing to make a mistake and then realize you did so because you just didn’t know any better. But it’s next-level shitbaggery to make a mistake and not even know it and then continue to believe you never made a mistake because you’re awesome.2
That is the Dunning-Kruger Effect. And that is what Russell says is so wrong with the world. The fact that we all do this. That we predictably overestimate our knowledge and abilities in a way that causes more errors and graver mistakes.
For example:

Gun owners who think they’re highly knowledgeable about gun safety score the lowest on tests of gun safety.3
Medical lab workers—the people who process samples for medical test results—who rate themselves as highly competent in their jobs are actually the worst at their jobs.4
Elderly people who think they’re better drivers than most are actually four times more likely to make unsafe driving errors.5
The lowest performing college students dramatically overestimate their performance on exams6 and their general knowledge in their area of study.7
The lowest performers in a debate competition wildly overestimated how well they did. They thought they’d won 59% of their contests when they actually only won 22% of them. 8

Yeah, but Mark, You Don’t Get It… I Really Am Awesome
Now, I know what you probably did when you read through that list. It’s probably the same thing I did.
“Psh, those other people are soooo dumb. Good thing I know about all of the ways I’m terrible at things… which means that the things I’m amazing at, I actually am pretty amazing.” 
We read things like this and at no point do we stop to consider the areas where we think we are great are also delusional. We fail to consider that we also fall victim to this blindspot.
Yet we do… oh, we do.
Just one example: we have blindspots when it comes to our emotional awareness. We might project our own bullshit onto other people, misplace our anger and judgement, shut down when we get uncomfortable, overcompensate for feelings of inferiority, let our jealousy get the best of us, be an insensitive prick without even realizing we’re being an insensitive prick, and so on. We all do it. All of us.
But some of us think we don’t do it nearly as much or as poorly as others. And yet, studies have shown that people who rank lowest on objective emotional intelligence tests think they’re far more emotionally aware than others.9 And it gets worse. These same people were more reluctant to listen to feedback about their scores and—AND—were much less likely to express interest in resources that would help them improve their emotional intelligence. Go figure.

And it’s not just with emotions, it’s with… well, everything.
People with the unhealthiest lifestyle habits rate themselves as comparatively healthier than they actually are.10
People who score poorly on cognitive reasoning and analytical thinking tests severely overestimate their cognitive and analytical abilities.11 Meanwhile, the people who score highly underestimate their performance.
People who hold the most politically biased views also hold the most inaccurate “factual” beliefs. They wildly over or underestimate numbers related to things like welfare participation and government budget expenditures.12
So here we are, trapped in a universe where the people who need the most help not only refuse it, they refuse to even believe they need help in the first place.
Are we just totally fucked? Or is there a way out?
The Paradox of Overcoming Ignorance
Perhaps the most frustrating thing about the Dunning-Kruger effect is that it’s incredibly difficult to overcome. And that’s because it’s wrapped in contradiction.
How do you get someone—or yourself—to look for something they can’t even see? How do you correct an error if you don’t even know you made one?
This is the paradox of trying to overcome our own ignorance: The very thing that would help us see our mistakes is the same thing that would keep us from making them in the first place.
You can’t reason with a conspiracy theorist precisely because they didn’t form their beliefs with reason. Had they the ability to change their beliefs based on reason and evidence, they wouldn’t have believed in wild conspiracy theories in the first place. In fact, they think they’re the only ones being reasonable to begin with.
Part of the problem is that there is comfort in the feeling of knowing. People don’t like uncertainty. And so settling on a belief helps us feel like we’ve made more sense of the world. When we can make sense of the world, we feel safe. Whether that belief is true or not doesn’t matter—it just has to give us some relief from the anxiety of not knowing.
Maybe there’s a backdoor way to infiltrate our stuck minds and unfuck them somehow. Research suggests it’s sorta-kinda-maybe possible.
Getting people to focus on developing related skills, rather than assessing their own abilities, seems to have some effect in reducing the Dunning-Kruger effect in task performance.13
For example, if someone is terrible at accounting but doesn’t realize it, perhaps you teach them organization skills so that in the process of learning how to better organize paperwork and transactions, they come to realize that they are the world’s worst accountant.
It also might be effective to simply teach people about the concept of blindspots and the Dunning-Kruger Effect to begin with and then let the idea percolate in their minds for a while until they start questioning their own assumptions.14
Also, as much as you’d like to be a dick to some of these people, it turns out it’s not helpful to ridicule them for how stupid they are.15 Ridiculing people simply causes them to become more defensive and double-down on their challenged beliefs, not relinquish them.
That said, you can gently peer pressure someone into seeing their ignorance. Try showing them examples of top performers in whatever field they’re so overly confident about.16 This might or might not work depending on how delusional the person is, but it’s worth a shot.
In the end, though, I think the only way to ward off our own ignorance is by choosing to have fewer opinions and more loosely held beliefs.
Humility is an important value. In fact, the Dunning-Kruger Effect suggests that humility can be highly practical. By intentionally underestimating our understanding of things, not only do we open up more opportunities to learn and grow, but we also foster a more realistic view of ourselves, and prevent ourselves from looking like a narcissistic assface around others.
That is… until we decide that we are the most humble person you’ve ever met. Nobody is more humble than me. I’m so much more humble than everybody else…
…and now we’re back to square one.

Footnotes

Dunning, D. (2011). The Dunning-Kruger effect: On being ignorant of one’s own ignorance. In J. M. Olson & M. P. Zanna (Eds.), Advances in Experimental Social Psychology (Vol. 44, pp. 247–296). Academic Press.↵
In psychology, this is known as the “double burden” of ignorance.↵
Ehrlinger, J., Johnson, K., Banner, M., Dunning, D., & Kruger, J. (2008). Why the unskilled are unaware: Further explorations of (absent) self-insight among the incompetent. Organizational Behavior and Human Decision Processes, 105(1), 98–121.↵
Haun, D. E., Zeringue, A., Leach, A., & Foley, A. (2000). Assessing the Competence of Specimen-Processing Personnel. Laboratory Medicine, 31(11), 633–637.↵
Freund, B., Colgrove, L. A., Burke, B. L., & McLeod, R. (2005). Self-rated driving performance among elderly drivers referred for driving evaluation. Accident Analysis & Prevention, 37(4), 613–618.↵
One study found that the poorest students thought they scored in the top 40% of their class when they actually scored in the bottom 15%. See: Ehrlinger, J., Johnson, K., Banner, M., Dunning, D., & Kruger, J. (2008). Why the unskilled are unaware: Further explorations of (absent) self-insight among the incompetent. Organizational Behavior and Human Decision Processes, 105(1), 98–121.↵
This includes a wide range of fields from medicine, nursing, biology, psychology, engineering, business, law, education, and management information systems. See: Mahmood, K. (2016). Do People Overestimate Their Information Literacy Skills? A Systematic Review of Empirical Evidence on the Dunning-Kruger Effect. Communications in Information Literacy, 10(2), 199.↵
Ehrlinger, J., Johnson, K., Banner, M., Dunning, D., & Kruger, J. (2008). Why the unskilled are unaware: Further explorations of (absent) self-insight among the incompetent. Organizational Behavior and Human Decision Processes, 105(1), 98–121.↵
Sheldon, O. J., Dunning, D., & Ames, D. R. (2014). Emotionally unskilled, unaware, and uninterested in learning more: Reactions to feedback about deficits in emotional intelligence. Journal of Applied Psychology, 99(1), 125–137.↵
Miller, J. E., Windschitl, P. D., Treat, T. A., & Scherer, A. M. (2019). Unhealthy and unaware? Misjudging social comparative standing for health-relevant behavior. Journal of Experimental Social Psychology, 85, 103873.↵
Pennycook, G., Ross, R. M., Koehler, D. J., & Fugelsang, J. A. (2017). Dunning–Kruger effects in reasoning: Theoretical implications of the failure to recognize incompetence. Psychonomic Bulletin & Review, 24(6), 1774–1784.↵
Kuklinski, J. H., Quirk, P. J., Jerit, J., Schwieder, D., & Rich, R. F. (2000). Misinformation and the Currency of Democratic Citizenship. The Journal of Politics, 62(3), 790–816.↵
Kruger, J., & Dunning, D. (1999). Unskilled and Unaware of It: How Difficulties in Recognizing One’s Own Incompetence Lead to Inflated Self-Assessments. Journal of Personality and Social Psychology, 77(6), 1121–1134.↵
You know, like I’m doing right now.↵
Sheldon, O. J., Dunning, D., & Ames, D. R. (2014). Emotionally unskilled, unaware, and uninterested in learning more: Reactions to feedback about deficits in emotional intelligence. Journal of Applied Psychology, 99(1), 125–137.↵
Miller, J. E., Windschitl, P. D., Treat, T. A., & Scherer, A. M. (2019). Unhealthy and unaware? Misjudging social comparative standing for health-relevant behavior. Journal of Experimental Social Psychology, 85, 103873.↵



